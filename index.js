const express = require("express");
const mongoose = require("mongoose");
const methodOverrride = require("method-override");
const Article = require("./models/article");

require("dotenv").config();

// ROUTES
const articleRoute = require("./routes/articles");

// CONNECT MONGODB
// mongoose.connect(
//   `mongodb+srv://lnx:ja0940D2BOI5Q0yU@lnx.hphr1pq.mongodb.net/blog`
// );
mongoose.connect(`${process.env.CONNECTION_STRING}/blog`);

const app = express();
const port = 8000;

//  SET OPTION FOR EXPRESSJS
app.set("view engine", "ejs");
app.use(express.urlencoded({ extended: false }));
app.use(methodOverrride("_method"));

// RENDER INDEX PAGE
app.get("/", async (req, res) => {
  const articles = await Article.find().sort({ createdAt: "desc" });
  res.render("articles/index", { articles: articles });
});

// USE ROUTES
app.use("/articles", articleRoute);

// LISTEN
app.listen(port, () => {
  console.log(`server is running on http://localhost:${port} ...`);
});
